#FUNÇÕES EM PYTHON

# 1. o que são funções e por que utiliza-las ?

# funções que ja utilizamos até agora:

# print() - imprime na tela
# input() - recebe dados do usuário
# type() - retorna o tipo de dado
# len() - retorna o tamanho de um dado
# int() - converte para inteiro
# float() - converte para real
# str() - converte para string
# bool() - converte para booleano
# list() - converte para lista
# dict() - converte para dicionário
# tuple() - converte para tupla
# set() - converte para conjunto
# max() - retorna o maior valor
# min() - retorna o menor valor
# sum() - retorna a soma dos valores
# sorted() - retorna uma lista ordenada
# range() - retorna uma sequência de números
# enumerate() - retorna um objeto enumerado
# zip() - retorna um objeto zipado
# open() - abre um arquivo
# help() - retorna a documentação de um dado

# 2. como criar uma função ?

# função inicia com def
# def nome_da_funcao():

def greeting():
    print('Olá, seja bem vindo(a)!')
    print('Espero que você esteja gostando do curso!')

# chamando a função
greeting()

# 3. como criar uma função com parâmetros ?
# def nome_da_funcao(parametro1, parametro2):

def welcome(nome, idade):
    print(f'Olá {nome}, você tem {idade} anos')

# chamando a função
welcome('Marcelo', 43)
welcome('Julia', 19)

# 4. como criar uma função com parâmetros opcionais ?
# def nome_da_funcao(parametro1, parametro2=valor_padrao):

def welcome(nome, idade=0):
    print(f'Olá {nome}, você tem {idade} anos')

# chamando a função
welcome('Marcelo')
welcome('Julia', 19)

# 5. como criar uma função com retorno ?
# def nome_da_funcao(parametro1, parametro2=valor_padrao):

def soma(n1, n2):
    return n1 + n2

# chamando a função
resultado = soma(10, 20)
print('O resultado da some é', resultado)

# 6. como criar uma função com retorno de múltiplos valores ?
# def nome_da_funcao(parametro1, parametro2=valor_padrao):

def soma_subtracao(n1, n2):
    return n1 + n2, n1 - n2

# chamando a função
resultado1 = soma_subtracao(10, 20)
print(resultado1)

resultado2, resultado3 = soma_subtracao(10, 20)
print(resultado2)

print(resultado3)

# 7. como criar uma função com parâmetros infinitos ?
# def nome_da_funcao(parametro1, parametro2=valor_padrao):

def soma(*numero):
    total = 0
    for n in numero:
        total += n
    return total

# chamando a função
resultado = soma(10, 20, 30, 40, 50)
print(resultado)

def calculadora(n1, n2, op):
    if op == 1:
        return n1 + n2
    elif op == 2:
        return n1 - n2
    elif op == 3:
        return n1 * n2
    elif op == 4:
        return n1 / n2
    elif op == 5:
        return 'Saindo...'
    else:
        return 'Opção inválida'

# chamando a função
value1 = int(input('Digite o primeiro número: '))
value2 = int(input('Digite o segundo número: '))
print('Escolha a operação desejada:')
print('1 - Soma')
print('2 - Subtração')
print('3 - Multiplicação')
print('4 - Divisão')
print('5 - Sair')
opera = int(input('Digite o numero da operação: '))
resultado = calculadora(value1, value2, opera)
print(resultado)