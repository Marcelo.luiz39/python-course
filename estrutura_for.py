# ESTRUTURA FOR
# FOR é uma estrutura de repetição que executa um bloco de código
# enquanto uma condição for verdadeira

# Exemplo 1
# Imprime os números de 0 a 9
for valor in range(10):
  print(valor)

print('Fim do programa')

# Exemplo 2
# Imprime os números de 1 a 5
for valor in range(1, 6):
  print(valor)

print('Fim do programa')

# Exemplo 3
# Imprime os números de 1 a 10, pulando de 2 em 2
for valor in range(1, 11, 2):
  print(valor)

print('Fim do programa')

# Exemplo 4
# Imprime os números de 10 a 1, pulando de 2 em 2
for valor in range(10, 0, -2):
  print(valor)

print('Fim do programa')

# Exemplo 5
#Usaremos o for para pegar notas de um aluno e calcular a média

soma = 0

for i in range(1, 4):
  nota = float(input(f'Digite a nota {i}: ')) #f-string

  soma += nota

print(f'A soma das notas é {soma} e a média é {soma / 3 :.1f}')