# MÉTODOS DE LISTAS

# append() - Adiciona um elemento ao final da lista
# clear() - Remove todos os elementos da lista
# copy() - Retorna uma cópia da lista
# count() - Retorna o número de elementos com o valor especificado
# extend() - Adiciona os elementos de uma lista (ou qualquer iterável), ao final da lista atual
# index() - Retorna o índice do primeiro elemento com o valor especificado
# insert() - Adiciona um elemento em uma posição especificada
# pop() - Remove o elemento de uma posição especificada
# remove() - Remove o primeiro item com o valor especificado
# reverse() - Inverte a ordem da lista
# sort() - Ordena a lista

# Exemplo 1
# append()
lista = [1, 3, 12, 8, 2, 5]

print('Antes do append:', lista)

lista.append(7)

print('Depois do append:', lista)

print('------------------------')

# Exemplo 2
# insert()
lista = [1, 3, 12, 8, 2, 5]

print('Antes do insert:', lista)

lista.insert(2, 99)

print('Depois do insert:', lista)

print('------------------------')

# Exemplo 3
# extend()
lista2 = [6, 7, 8, 9, 10]

print('Antes do extend:', lista)

lista.extend(lista2)

print('Depois do extend:', lista)

print('------------------------')

# Exemplo 4
# pop()
lista = [1, 3, 12, 8, 2, 5]

print('Antes do pop:', lista)

lista.pop(2)

print('Depois do pop:', lista)

print('------------------------')

# Exemplo 5
# remove()

lista = [1, 3, 12, 8, 2, 5]

print('Antes do remove:', lista)

lista.remove(12)

print('Depois do remove:', lista)

print('------------------------')

# Exemplo 6
# clear()

lista = [1, 3, 12, 8, 2, 5]

print('Antes do clear:', lista)

lista.clear()

print('Depois do clear a lista ficou:', lista)

print('------------------------')

# Exemplo 7
# index()

lista = [1, 3, 12, 8, 2, 5]

print('Index do elemento 8 =>', lista.index(8))

print('------------------------')

# Exemplo 8
# count()

lista = [2, 1, 3, 12, 8, 2, 5, 6, 2]

print('Quantidade de números 2 na lista:', lista.count(2))

print('------------------------')

# Exemplo 9
# sort()

lista = [1, 3, 12, 8, 2, 5]

print('Antes do sort:', lista)

lista.sort()

print('Depois do sort:', lista)

print('------------------------')

# Exemplo 10
# reverse()

lista = [1, 3, 12, 8, 2, 5]

print('Antes do reverse:', lista)

lista.sort(reverse=True)

print('Depois do reverse:', lista)

print('------------------------')

# Exemplo 11
# copy()

lista = [1, 3, 12, 8, 2, 5]

print('Antes do copy:', lista)

lista3 = [55,32,78,12,45,99]

lista = lista3.copy()

print('Depois do copy:', lista)

print('------------------------')

#FUNÇÕES PARA LISTAS

# len() - Retorna o tamanho da lista
# sum() - Retorna a soma dos valores da lista
# max() - Retorna o maior valor da lista
# min() - Retorna o menor valor da lista

# Exemplo 1
# len()

lista = [1, 3, 12, 8, 2, 5]

print('Tamanho da lista:', len(lista))

print('------------------------')

# Exemplo 2
# sum()

lista = [1, 1, 1, 1, 1, 1]

print('Soma dos valores da lista:', sum(lista))

print('------------------------')

# Exemplo 3
# max()

lista = [1, 3, 12, 8, 2, 5]

print('Maior valor da lista:', max(lista))

print('------------------------')

# Exemplo 4
# min()

lista = [1, 3, 12, 8, 2, 5]

print('Menor valor da lista:', min(lista))

print('------------------------')


