# Essa linha é um comentário
# O Python ignora tudo que estiver após o símbolo '#'

# O Python ignora tudo que estiver entre aspas triplas
# """ ou '''

"""
Essa linha é um comentário
"""

'''
Essa linha é um comentário
'''