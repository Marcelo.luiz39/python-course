# DICIONÁRIOS - DICT

""" Dicionários são estruturas de dados que permitem armazenar dados de forma
organizada e acessar esses dados de forma mais rápida.
"""

""" Dicionários são representados por chaves {} e cada elemento é composto por
uma chave e um valor, separados por dois pontos :.
"""
# Dicionários são mutáveis, ou seja, podemos adicionar, remover e alterar elementos.

# Exemplo:
# dicionario = {'chave1': 'valor1', 'chave2': 'valor2'}
dicionario = {}
dicionario = dict()

dicionario = {'nome': 'Marcelo', 'idade': 43}

print(dicionario)

# Acessando elementos
print(dicionario['nome'])

# Acessando elementos com get
print(dicionario.get('idade'))

#Adicionando elementos
dicionario['programador'] = True

print(dicionario)

dicionario['nome'] = 'Marcelo Luiz'

print(dicionario)

dicionario['cursos'] = ['programação', 'python', 'web']

print(dicionario)

#Percorrendo dicionários
for chave in dicionario:
    print(chave, ':', dicionario[chave])

#Testando se uma chave existe
print('nome' in dicionario)
peso = 'peso'
if peso in dicionario:
    print(f'Existe uma chave {peso} no dicionário')
else:
    print(f'Não existe uma chave {peso} no dicionário')

print(dicionario.keys())


