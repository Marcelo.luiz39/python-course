# CONVERSÃO DE TIPOS
# 1. Conversão de tipos explícita
# 2. Conversão de tipos implícita
# 3. Conversão de tipos com funções
# 4. Conversão de tipos com operadores
# 5. Conversão de tipos com métodos
# 6. Conversão de tipos com classes
# 7. Conversão de tipos com módulos
# 8. Conversão de tipos com pacotes
# 9. Conversão de tipos com bibliotecas
# 10. Conversão de tipos com frameworks
# 11. Conversão de tipos com APIs
# 12. Conversão de tipos com bibliotecas de terceiros

idade = '26'
numero1 = '10'
numero2 = '20'
numero3 = 30
idade_inteira = int(idade)

print(numero1 + numero2)
print(idade, type(idade)) # Conversão de tipos implícita
print(idade_inteira, type(idade_inteira)) # Conversão de tipos explícita
print(numero3 + int(numero1)) # Conversão de tipos explícita
# print(numero2 + numero3)

# int() - Conversão de tipos explícita
# float() - Conversão de tipos explícita
# str() - Conversão de tipos explícita
# bool() - Conversão de tipos explícita

altura = input('informe sua altura: ') # aqui o input sempre retorna uma string
print(altura, type(altura))
print(float(altura), type(float(altura)))
