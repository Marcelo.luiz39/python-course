# ESTRUTURAS DE CONTROLE DE FLUXO

# 1. Condicional simples
# 2. Condicional composta
# 3. Condicional aninhada
# 4. Condicional encadeada
# 5. Condicional encadeada com elif
# 6. Condicional encadeada com elif e else
# 7. Condicional encadeada com elif, else e operador ternário
# 8. Condicional encadeada com elif, else, operador ternário e operador lógico
# 9. Condicional encadeada com elif, else, operador ternário, operador lógico e operador de comparação
# 10. Condicional encadeada com elif, else, operador ternário, operador lógico, operador de comparação e operador de identidade
# 11. Condicional encadeada com elif, else, operador ternário, operador lógico, operador de comparação, operador de identidade e operador de associação

"""
Imagine que você está desenvolvendo um sistema de cadastro de clientes e você precisa validar se o cliente é maior de idade ou não.

Imagine que você queira imprimir "Aprovado(a)", caso o estudante tenha uma média superior ou igual a 7 e "Reprovado" se a media for menor que 7.
"""

idade = int( input('Informe sua idade: '))

if idade >= 18:
    print('Você é maior de idade pode seguir com a media do curso!')
    media = float( input('Informe a média do estudante: '))
    if media >= 7:
      print('Você foi aprovado(a)')
    elif media >= 5:
      print('Você está de recuperação')
    else:
      print('Você foi reprovado(a)')
else:
    print('Você é menor de idade não está na faculdade ainda!')

media_faculdade = float( input('Informe a média do estudante: '))
presence = float( input('Informe a presença do estudante: '))

if media_faculdade >= 7 and presence >= 75:
  print('Você foi aprovado(a)')
elif media_faculdade >= 5 and presence >= 75:
  print('Você está de recuperação')
else:
  print('Você foi reprovado(a)')

