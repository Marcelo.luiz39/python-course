# > Operações Matemáticas ( Aritméticas )

"""
- Soma: +
- Subtração: -
- Multiplicação: *
- Divisão: /
- Divisão inteira: //
- Resto da divisão: %
- Potência: **
"""

numero1 = 10
numero2 = 20

soma = numero1 + numero2
subtracao = numero1 - numero2
multiplicacao = numero1 * numero2
divisao = numero1 / numero2
divisao2 = numero1 // numero2
divisao_inteira = 20 // 3
resto_divisao = 20 % 3
potencia = 2 ** 3

print(soma)
print(subtracao)
print(multiplicacao)
print(divisao)
print(divisao2)
print(divisao_inteira)
print(resto_divisao)
print(potencia)

# Operadores Booleanos

idade1 = 18
idade2 = 20
altura1 = 1.80
altura2 = 1.70
altura3 = altura2

# - Maior que: >
# - Menor que: <
# - Maior ou igual: >=
# - Menor ou igual: <=
# - Igual: ==
# - Diferente: !=
# - E: and
# - Ou: or
# - Não: not

print(idade1 > idade2)
print(idade1 < idade2)
print(altura1 >= altura2)
print(altura1 <= altura2)
print(altura2 == altura3)
print(altura2 > altura3)
print(altura2 < altura3)
print('Python' == 'python')
print(altura1 != altura2)
print('banana' != 'abacaxi')
print(idade1 > 18 and altura1 >= 1.80)
print(idade1 > 18 or altura1 >= 1.80)
print(not idade1 > 18)
