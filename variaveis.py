# Description: Variáveis
"""
1 int - inteiro (números inteiros)
2 float - real (números reais)
3 str - string (texto)
4 bool - booleano (True ou False) e diferente de outras linguagens
tem que ser escrito com a primeira letra maiúscula
Ou seja no python ele é case sensitive

# Exemplos:
idade = 43
altura = 1.75
nome = "João"
casado = True
"""
idade = 43
altura = 1.75
nome = "Marcelo"
casado = False

print("Olá, Mundo!")
name = input("Qual é o seu nome ? \n")

print("Olá,", name) # aqui o nome da variável é passado como parâmetro para o método print() por meio de vírgula

print("Olá, " + name + "!") # aqui o nome da variável é passado como parâmetro para o método print() por meio de concatenação

print("Olá, {}!".format(name)) # aqui o nome da variável é passado como parâmetro para o método format()

print(f"Olá, {name}!") # aqui o nome da variável é passado como parâmetro para o método format() usando f-string

