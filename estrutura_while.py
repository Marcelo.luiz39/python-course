# ESTRUTURA DE REPETIÇÃO WHILE

# A estrutura de repetição while é uma estrutura de controle de fluxo que permite executar um bloco de código enquanto uma condição for verdadeira.

import random

numero_sorteado = round( random.random() * 10 )
numero_chutado = int( input('Chute um número de 0 a 10: ') )

while numero_chutado != numero_sorteado:
  numero_chutado = int( input('errado chute novamente um número entre 0 a 10: ') )
print('Parabéns você acertou o número sorteado!')

# Exemplo 2 estrutura com contador

contador = 0

while contador <= 10:
  print(contador)
  contador = contador + 1

# Exemplo 3 estrutura com contador e break

contador = 0

while contador < 10:
  print(contador)
  contador = contador + 1
  if contador == 5:
    break

# Exemplo 4 estrutura com contador e continue

contador = 0

while contador < 10:
  contador = contador + 1
  if contador == 5:
    continue
  print(contador)

# Exemplo 5 estrutura com contador e else

contador = 0

while contador < 10:
  print(contador)
  contador = contador + 1

else:
  print('O contador chegou ao valor 10')



